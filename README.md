# corona-help.org

Corona-Help.org is an initiative of volunteers.
The initiative has the goal of connecting People in Need (elderly people, high risk people) with volunteers,
which offer to go shopping, dog walking etc. to protect the People in Need to get in contact with the environment too
much.

The goals of the project:
  * Offer a phone hotline for People in Need (elderly people, high risk people) for registering as Person in Need
  * Offer a web application to Street Volunteers where they can find and connect to People in Need 

This repository contains the web portal available at https://corona-help.org/.


## Participate and Contribute!

**We are searching for volunteers! Get in contact via our [Slack Channel](https://join.slack.com/t/corona-helporg/shared_invite/zt-cmjlji0i-YB2rGq66MgQnm_oguspCOA)!**

**Feel free to contact our [team members](CONTRIBUTORS.md) to learn where you can help.**

Alternatively, you can directly start to work on [open issues](https://gitlab.com/corona-help.org/portal/-/issues)
or [create new issues](https://gitlab.com/corona-help.org/portal/-/issues/new), if you're missing something on [corona-help.org](https://corona-help.org).

See our [contribution guidelines](CONTRIBUTING.md) for more information on how to contribute.

## Branch Overview

  * `master` -- Main Development Branch, current state deployed on https://dev.corona.help.org/
  * `staging` -- Branch of Code running on the Staging environment, deployed on https://staging.corona-help.org/
  * `production` -- Branch of Code running on the Production environment, https://corona-help.org/


## License Information - GPLv3

```
Corona-Help.org Volunteer Initiative Web Portal
Copyright (C) 2020 Corona-Help.org Developers

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
```

You can read the [full license text here](LICENSE.md).
