# Corona-Help.org Volunteer Initiative Web Portal
# Copyright (C) 2020 Corona-Help.org Developers
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from django.http import HttpResponseRedirect
from django.shortcuts import get_object_or_404, redirect
from django.contrib.auth.mixins import LoginRequiredMixin
from django.urls import reverse_lazy
from django.views.generic import UpdateView
from django_registration.backends.activation.views import RegistrationView
from twilio.rest import Client
from django.conf import settings
from .forms import HelperForm, SmSverifyForm
import random
import string
from .models import User


def generate_sms_token(stringLength):
    letters = string.ascii_lowercase
    return ''.join(random.choice(letters) for i in range(stringLength))


class HelperCreateView(RegistrationView):
    model = User
    form_class = HelperForm
    success_url = reverse_lazy('thankyou')

    def form_valid(self, form):
        return HttpResponseRedirect(self.get_success_url(self.register(form)))

    def send_sms_token(self, user):
        user.sms_verifyer_token = generate_sms_token(5)
        user.save()
        if settings.DEBUG:
            print("sms token will be: "+user.sms_verifyer_token)

        else:
            account_sid = settings.TWILIO_ACCOUNT_SID
            auth_token = settings.TWILIO_TOKEN
            client = Client(account_sid, auth_token)

            client.messages.create(
                body='Willkommen bei corona-help.org. Dein Token lautet: '+user.sms_verifyer_token,
                from_=settings.TWILIO_FON_NUMBER,
                to=user.fon
            )

    def register(self, form):
        """
        Implement user-registration logic here. Access to both the
        request and the registration form is available here.

        """
        if form.cleaned_data.get('password1') == form.cleaned_data.get('password2'):
            user = User.objects.create_user(form.cleaned_data)
            self.send_activation_email(user)
            self.send_sms_token(user)


class UserIsSmSverified(object):
    user_check_failure_path = 'sms_verify_from'  # can be path, url name or reverse_lazy

    def check_user(self):
        if self.request.user.sms_verified:
            return True

    def user_check_failed(self, request, *args, **kwargs):
        return redirect(self.user_check_failure_path)

    def dispatch(self, request, *args, **kwargs):
        if not self.check_user():
            return self.user_check_failed(request, *args, **kwargs)
        return super(UserIsSmSverified, self).dispatch(request, *args, **kwargs)


class SmSverifyerView(LoginRequiredMixin, UpdateView):

    template_name = "registration/sms_verification_form.html"
    model = User
    form_class = SmSverifyForm
    success_url = reverse_lazy('localactivists__main_page')

    def dispatch(self, request, *args, **kwargs):
        # check if there is some video onsite
        if not self.queryset:
            return redirect('other_page')
        else:
            return super(SmSverifyerView, self).dispatch(request, *args, **kwargs)

    def get_object(self, *args, **kwargs):
        user = get_object_or_404(User, pk=self.request.user.pk)
        return user

    def post(self, request, *args, **kwargs):
        self.object = self.get_object()
        if self.object.sms_verifyer_token == self.request.POST.get('sms_token'):
            self.object.sms_verified = True
            self.object.save()
            return super().post(request, *args, **kwargs)
        return super().post(request, *args, **kwargs)
