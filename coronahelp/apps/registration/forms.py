# Corona-Help.org Volunteer Initiative Web Portal
# Copyright (C) 2020 Corona-Help.org Developers
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
from django.core.exceptions import ValidationError
from django.forms import ModelForm
from django_registration.forms import RegistrationForm, forms
from .models import User
from ..public.models import LANGUAGES
from django.utils.translation import ugettext_lazy as _


class HelperForm(RegistrationForm):
    languages = forms.MultipleChoiceField(choices=LANGUAGES, widget=forms.CheckboxSelectMultiple())

    class Meta(RegistrationForm.Meta):
        model = User
        fields = ['first_name',
                  'last_name',
                  'email',
                  'fon',
                  'street_name',
                  'street_number',
                  'postal_code',
                  'city',
                  'languages']


class SmSverifyForm(ModelForm):
    sms_token = forms.CharField(max_length=10, label=_("Please enter SMS Token"))

    class Meta:
        model = User
        fields = ["sms_token"]

    def clean(self):

        sms_token = self.cleaned_data.get('sms_token')
        if sms_token == self.instance.sms_verifyer_token:
            return self.cleaned_data
        else:
            self.add_error(None, ValidationError(_("Token is invalid")))
