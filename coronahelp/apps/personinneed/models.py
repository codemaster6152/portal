# Corona-Help.org Volunteer Initiative Web Portal
# Copyright (C) 2020 Corona-Help.org Developers
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from django.db import models
from django.utils.translation import ugettext_lazy as _
import math
from ..registration.models import User, CATEGORIES
from ..public.models import Languages


class HelpCategories(models.Model):
    name = models.CharField(choices=CATEGORIES, max_length=20)

    def __str__(self):
        return self.name


class PersonInNeed(models.Model):
    created = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    first_name = models.CharField(max_length=100, verbose_name="Vorname")
    last_name = models.CharField(max_length=255, verbose_name="Nachname")
    fon = models.CharField(max_length=100, verbose_name="Telefonnummer",
                           help_text="Bitte im Format +49(erste Null weglassen) 152337182 etc.")
    postal_code = models.IntegerField(verbose_name="PLZ")
    street_name = models.CharField(max_length=255, verbose_name="Straßenname")
    street_number = models.CharField(max_length=10, verbose_name="Hausnummer")
    city = models.CharField(max_length=255, verbose_name=_("Ort"))
    state = models.CharField(max_length=255, verbose_name=_("Bundesland"))
    country = models.CharField(max_length=255, verbose_name=_("Land"))
    email_address = models.EmailField(null=True, blank=True)
    language = models.ManyToManyField(Languages, related_name="lang_pin")
    lat = models.FloatField(verbose_name="latitude", null=True, blank=True)
    long = models.FloatField(verbose_name="longitude", null=True, blank=True)

    def __str__(self):
        return self.first_name + self.last_name

    @staticmethod
    def search_by_radius(lat, long, radius, max_results=None):
        """
        For simplicity reasons: Fetching rectangle instead of circle
        https://www.kompf.de/gps/distcalc.html
        lat/long - geo coordinates
        radius   - km
        """
        d_lat = radius / 111.3
        d_long = radius / (111.3 * math.cos(lat * math.pi / 180))
        # return PersonInNeed.objects.filter(
        #     long__range=(long - d_long, long + d_long),
        #     lat__range=(lat - d_lat, lat + d_lat)
        # )[:max_results]
        print(d_long, d_lat)
        return PersonInNeed.objects.filter(
            lat__range=(lat - d_lat, lat + d_lat),
            long__range=(long - d_long, long + d_long),
            tasks__status="open"
        )[:max_results]


class Tasks(models.Model):
    STATUS_CHOICES = (
        ('ongoing', _('wird gerade erledigt')),
        ('done', _('erledigt')),
        ('open', _('offen'))
    )

    class TaskChoices(models.TextChoices):
        ONCE = 'once', _('einmalig')
        RECURRING = 'RC', _('regelmäßig')

    created = models.DateTimeField(auto_now_add=True)
    last_updated = models.DateTimeField(auto_now=True)
    title = models.CharField(max_length=255, verbose_name=_("Aufgabe"))
    description = models.TextField(blank=True)
    type = models.CharField(max_length=10, choices=TaskChoices.choices,
                            default=TaskChoices.ONCE, verbose_name=_("aufgabenart"))
    status = models.CharField(choices=STATUS_CHOICES, max_length=30)
    helper = models.ForeignKey(User, null=True, blank=True, on_delete=models.CASCADE)
    person_in_need = models.ForeignKey(PersonInNeed, null=True, blank=True, on_delete=models.CASCADE,
                                       related_name='tasks')
    help_topics = models.ForeignKey("HelpCategories", on_delete=models.CASCADE, verbose_name=_("Hilfethema"), null=True)

    def __str__(self):
        return self.title
