# Corona-Help.org Volunteer Initiative Web Portal
# Copyright (C) 2020 Corona-Help.org Developers
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from rest_framework import serializers

from ..personinneed.models import PersonInNeed, Tasks


class HelpTopicSerializer(serializers.ModelSerializer):
    name = serializers.CharField(max_length=200)

    class Meta:
        model = Tasks
        fields = ['name']


class TaskSerializer(serializers.ModelSerializer):
    id = serializers.IntegerField()
    title = serializers.CharField(max_length=200)
    type = serializers.CharField(max_length=10)
    help_topics = HelpTopicSerializer()

    class Meta:
        model = Tasks
        fields = ['id', 'title', 'status', 'type', 'help_topics']


class PeopleInNeedInitialNotLoggedIn(serializers.ModelSerializer):
    tasks = TaskSerializer(many=True, read_only=True)

    class Meta:
        model = PersonInNeed
        fields = ['id', 'lat', 'long', 'tasks', 'postal_code', 'street_name']
        depth = 3
