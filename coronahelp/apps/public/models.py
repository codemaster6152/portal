# Corona-Help.org Volunteer Initiative Web Portal
# Copyright (C) 2020 Corona-Help.org Developers
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
from django.utils.translation import ugettext_lazy as _
from django.db import models


LANGUAGES = (("de", _("Deutsch")),
             ("en", _("Englisch")),
             ("arb", _("Arabisch")),
             ("es", _("Spanisch")),
             ("rus", _("Russisch"))
             )


class Languages(models.Model):
    name = models.CharField(choices=LANGUAGES, max_length=10)

    def __str__(self):
        return self.name
