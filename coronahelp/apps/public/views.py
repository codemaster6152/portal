# Corona-Help.org Volunteer Initiative Web Portal
# Copyright (C) 2020 Corona-Help.org Developers
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from django.views.generic import TemplateView
from rest_framework.decorators import api_view, authentication_classes, permission_classes
from rest_framework.response import Response

from .serializers import PeopleInNeedInitialNotLoggedIn

from coronahelp.apps.personinneed.models import PersonInNeed


class LandingPage(TemplateView):
    template_name = 'public/home.html'

    def get(self, request, *args, **kwargs):
        context = self.get_context_data(**kwargs)
        return self.render_to_response(context)


@api_view(['GET'])
@authentication_classes([])
@permission_classes([])
def api_initial_data(request):

    '''
    API endpoint for not authorized users on the landing page
    '''

    if request.method == 'GET':
        pin_nextby = PersonInNeed.search_by_radius(float(request.GET.get('lat')),
                                                   float(request.GET.get('lng')), 240, 200)
        pin_serializer = PeopleInNeedInitialNotLoggedIn(pin_nextby, many=True)
        print(pin_serializer.data)

        return Response(pin_serializer.data)
