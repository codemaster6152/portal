
from rest_framework import serializers
from coronahelp.apps.personinneed.models import PersonInNeed, Tasks
from coronahelp.apps.registration.models import User


class HelpTopicSerializerAuthorized(serializers.ModelSerializer):
    name = serializers.CharField(max_length=200)

    class Meta:
        model = Tasks
        fields = ['name']


class TaskSerializerAuthorized(serializers.ModelSerializer):
    id = serializers.IntegerField()
    title = serializers.CharField(max_length=200)
    type = serializers.CharField(max_length=10)
    help_topics = HelpTopicSerializerAuthorized()

    class Meta:
        model = Tasks
        fields = ['id', 'title', 'type', 'help_topics']


class PeopleInNeedInitialAuthorized(serializers.ModelSerializer):
    tasks = TaskSerializerAuthorized(many=True, read_only=True)

    class Meta:
        model = PersonInNeed
        fields = ['id', 'lat', 'long', 'tasks', 'street_name', 'street_number',
                  'postal_code', 'city', 'state', 'country', 'language']
        depth = 3


class PeopleInNeedFonNumberAuthorized(serializers.ModelSerializer):
    tasks = TaskSerializerAuthorized(many=True, read_only=True)

    class Meta:
        model = PersonInNeed
        fields = ['fon']
        depth = 1


class TaskSerializer(serializers.ModelSerializer):
    class Meta:
        model = Tasks
        fields = ['id', 'title', 'status', 'description']
        extra_kwargs = {
            'id': {'read_only': True},
            'title': {'read_only': True},
            'description': {'read_only': True},
        }


class MyPersonsInNeed(serializers.ModelSerializer):
    tasks = TaskSerializerAuthorized(many=True, read_only=True)

    class Meta:
        model = PersonInNeed
        fields = ['id', 'lat', 'long', 'fon', 'tasks', 'street_name',
                  'street_number', 'postal_code', 'city', 'state', 'country', 'language']
        depth = 1


class MyTasksSetSerializer(serializers.ModelSerializer):
    person_in_need = MyPersonsInNeed(read_only=True)

    class Meta:
        model = Tasks
        fields = ['id', 'title', 'status', 'description', 'person_in_need']


class UserSerializer(serializers.ModelSerializer):
    get_my_open_tasks = MyTasksSetSerializer(read_only=True, many=True)

    class Meta:
        model = User
        fields = ['first_name', 'get_my_open_tasks']
        depth = 3
