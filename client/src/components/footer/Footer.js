import React from 'react';
import { useTranslation } from 'react-i18next';
import './styles.scss';

const Footer = () => {
    const { t, i18n } = useTranslation();

    const changeLanguage = ev => {
        console.log(ev.currentTarget.value);

        i18n.changeLanguage(ev.currentTarget.value);
    }

    return (
        <footer className="footer">
            <a href="#">{t('footer.impressum')}</a>
            <a href="#">{t('footer.datenschutz')}</a>
            <a href="#">{t('footer.cookies')}</a>
            <a href="#">{t('footer.nutzungsbestimmungen')}</a>
            <select id="language" name="language" onChange={changeLanguage} value={i18n.language}>
                <option value="de">DE </option>
                <option value="en">EN </option>
            </select>
        </footer >
    );
}

export default Footer;