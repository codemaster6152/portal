import React from 'react';
import { useTranslation } from 'react-i18next';
import './styles.scss';

const CookieConsent = () => {
    const { t } = useTranslation();

    return (
        <div className="cookies">
            <p> {t('cookieConsent.useCookies')} <a href="#"> {t('cookieConsent.moreInfo')}</a></p>
            <button type="submit" className="btn btn-cookies">{t('cookieConsent.reject')}</button>
            <button type="submit" className="btn btn-cookies">{t('cookieConsent.accept')}</button>
        </div>
    );
}

export default CookieConsent;