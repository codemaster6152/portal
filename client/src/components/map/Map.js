import React, { useEffect, useState, useRef } from 'react';
import 'leaflet/dist/leaflet.css';
import L from 'leaflet';
import _ from 'underscore'
import { MAP_API_KEY } from '../../config';
import { API_SERVER } from '../../config';
import TaskBox from "../taskbox";
import axios from "axios";
import './styles.scss';
import {useTranslation} from "react-i18next";

delete L.Icon.Default.prototype._getIconUrl;
L.Icon.Default.mergeOptions({
    iconRetinaUrl: require('../../styles/images/marker.svg'),
    iconUrl: require('../../styles/images/marker.svg'),
});

const Map = ({ tasktype }) => {
    const { t } = useTranslation();
    const [markerData, setMarkerData] = useState([]);
    const mymap = useRef();

    const fetchMarkers = async () => {
        const result = await axios.get(API_SERVER + 'api/v1/initial_data/?lat=50.9552737&lng=11.0820338&format=json&type=' + tasktype);
        const markerData = result.data;
        setMarkerData(markerData);
    }

    useEffect(() => {


        mymap.current = L.map('mapid').setView([52.5127808, 13.3898995], 14);

        L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token={accessToken}', {
            attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
            maxZoom: 18,
            id: 'mapbox/streets-v11',
            tileSize: 512,
            zoomOffset: -1,
            accessToken: MAP_API_KEY
        }).addTo(mymap.current);

        fetchMarkers();
    }, []);

    useEffect(() => {
        const icon_paths = {
            "someone_needs":require('../../styles/images/dog.svg'),
            "shopping":require('../../styles/images/shopping-cart.svg'),
            "animal_care":require('../../styles/images/dog.svg'),
            "fon_care":require('../../styles/images/phone.svg'),
            "logistics":require('../../styles/images/truck.svg'),
            "babysitting":require('../../styles/images/smile.svg'),
            "other":require('../../styles/images/star.svg'),
        };


        const unselectedmyIcon = L.icon({
            iconUrl: require('../../styles/images/marker.svg'),
            iconRetinaUrl: require('../../styles/images/marker.svg'),
            iconSize: [30, 40],
            iconAnchor: [30, 20],
            popupAnchor: [-12, -15],
            shadowSize: [68, 95],
            shadowAnchor: [22, 94]
        });
        function createMarkerFromItem(item, map){
            const content = document.createElement('div');
            content.className="innerPopup";
            const header2 = document.createElement("h2");
            header2.textContent = "Jemand benötigt:";
            content.appendChild(header2);
            _.each(item.tasks, (thetask) => {
                if (thetask.status === "open"){

                    const taskContainer = document.createElement('div');
                    taskContainer.className = 'taskContainer';
                    const icon = document.createElement('img');
                    icon.src = icon_paths[thetask.help_topics.name];
                    taskContainer.appendChild(icon);
                    const taskSpn = document.createElement("span");
                    taskSpn.className = 'taskSpn';
                    const langTrans = 'tasks.'+thetask.help_topics.name;
                    taskSpn.innerText = t(langTrans);
                    taskContainer.appendChild(taskSpn);
                    content.appendChild(taskContainer);
                }
            });
            const button = document.createElement('button');
            button.textContent = "Hilf hier!";
            button.className = "action-button btn";
            button.onclick = ()=> {window.location=/registration/};
            content.appendChild(button);
            const marker =  L.marker([item.lat, item.long],{icon:unselectedmyIcon} ).addTo(map.current);
            marker.bindPopup(content);
        }



        _.each(markerData, function (item) {
            createMarkerFromItem(item, mymap)



        });
    }, [markerData]);

    return (
        <div>
            <TaskBox/>
            <div id="mapid" style={{height: '700px'}}></div>

        </div>
    )
}

export default Map;