import React from 'react';
import {useTranslation} from 'react-i18next';
import './styles.scss';
import TaskItemBox from "./taskitembox";

const TaskBox = () => {
    const {t, i18n} = useTranslation();

    return (
        <div id="outerTaskBox">
            <div id="taskBox">
                <h2>
                    Hier kannst Du helfen:
                </h2>
                <TaskItemBox/>
                <TaskItemBox/>
                <TaskItemBox/>
                <TaskItemBox/>
            </div>
        </div>
    );
};

export default TaskBox;