import React from 'react';
import './styles.scss';
import { useTranslation } from 'react-i18next';
import { RefreshCw, ChevronRight} from "react-feather";
import Description from "./description";

const TaskItemBox = () => {
    const { t } = useTranslation();
    return (
        <div id="taskItemBox">
            <RefreshCw size="14" /><span className="tasktype">{t('localactivist.recurring_task')}</span>
            <h3>Task Item Box yeah</h3>
            <Description />

        <button className="btn">{t('localactivist.help_here')}</button>
        </div>

    );
};
export default TaskItemBox;