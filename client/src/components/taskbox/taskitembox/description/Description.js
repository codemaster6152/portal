import React, {useState} from 'react';
import './styles.scss';
import {useTranslation} from 'react-i18next';
import {ChevronRight, ChevronDown} from "react-feather";


const Description = () => {
    const {t} = useTranslation();
    const [isToggled, setToggled] = useState(false);
    const toggleTrueFalse = () => setToggled(!isToggled);

    return (
        <div>
            {
            isToggled ? <ChevronDown className="arrow" size="20"/> :
            <ChevronRight className="arrow" size="20"/>
            }
            <span className="moreInformation" onClick={toggleTrueFalse}>{t('localactivist.more_information')}</span>
            <div className={`${isToggled ? "description open" : "description closed"}`}>
                <p>ljfkljdskflf jllafj dldsjf fhjdshk jfhsdjkf hjkd fk hfdsjksdf hjk hds}</p>
            </div>


        </div>

    );
};
export default Description;


