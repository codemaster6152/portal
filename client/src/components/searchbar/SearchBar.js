import React, { useState } from 'react';
import {
    InputGroup,
    InputGroupAddon,
    InputGroupText,
    Input,
    Button
} from 'reactstrap';
import { MapPin } from 'react-feather';
import { useTranslation } from 'react-i18next';
import FilterDropDown from './FilterDropDown';
import map from '../map'
import './styles.scss';

const SearchBar = () => {
    const { t } = useTranslation();
    const [selectedFilters, setSelectedFilters] = useState([]);
    //const [themap, setTheMap] = useState([])


    const options = [
        { name: t('tasks.someone_needs'), id: 'someone_needs' },
        { name: t('tasks.shopping'), id: 'shopping' },
        { name: t('tasks.animal_care'), id: 'animal_care' },
        { name: t('tasks.fon_care'), id: 'fon_care' },
        { name: t('tasks.logistics'), id: 'logistics' },
        { name: t('tasks.babysitting'), id: 'babysitting' },
        { name: t('tasks.other'), id: 'other' },
    ];

    const handleFilterChange = (values) => {
        // Maybe call API here
        setSelectedFilters(values);
        //setTheMap(map(values))
    };

    return (
        <div className="searchbar">
            <h2 className="searchbar__title">{t('searchbar.title')}</h2>
            <div className="filter-wrapper">
                <InputGroup className="searchbar__inputgroup">
                    <InputGroupAddon addonType="prepend">
                        <InputGroupText className="searchbar__input-icon">
                            <MapPin size="20" />
                        </InputGroupText>
                    </InputGroupAddon>
                    <Input
                        className="searchbar__input"
                        placeholder={t('searchbar.inputPlaceholder')}
                    />
                </InputGroup>
                <FilterDropDown options={options} selectedFilters={selectedFilters} onChangeFilter={handleFilterChange} />
            </div>
        </div >
    );
};

export default SearchBar;
