import React from 'react';
import { Square, CheckSquare } from 'react-feather';
import { Picky } from 'react-picky';

const FilterDropDown = ({ options, selectedFilters, onChangeFilter }) => (
    <Picky
        value={selectedFilters}
        onChange={onChangeFilter}
        options={options}
        valueKey="id"
        labelKey="name"
        multiple={true}
        numberDisplayed={2}
        includeSelectAll={true}
        render={({
            style,
            isSelected,
            item,
            selectValue,
            labelKey,
            valueKey,
        }) => (
                <li
                    style={style} // required
                    key={item[valueKey]} // required
                    onClick={() => selectValue(item)}
                    className={isSelected ? 'selected' : ''}
                >
                    {!isSelected ? <Square size="20" /> : <CheckSquare size="20" />}
                    <span>{item[labelKey]}</span>
                </li>
            )
        }
        renderSelectAll={({
            allSelected,
            toggleSelectAll,
        }) => (
                <li
                    onClick={toggleSelectAll}
                >
                    {allSelected !== 'all' ? <Square size="20" /> : <CheckSquare size="20" />}
                    <span>All</span>
                </li>
            )
        }
    />
);

export default FilterDropDown;
