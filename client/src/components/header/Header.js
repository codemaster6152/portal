import React, { useEffect, useState } from "react";
import axios from 'axios';
import { useTranslation } from "react-i18next";
import { Phone, Info } from "react-feather";
import Logo from "../../styles/images/logo.svg"
import './styles.scss';

const Header = () => {
    const { t } = useTranslation();
    const [userData, setUserData] = useState([])

    useEffect(() => {
        const fetchUserData = async () => {
            const result = await axios.get('http://127.0.0.1:8000/localactivist/api/v1/user/?format=json');
            const userData = result.data.results[0].first_name;
            setUserData(userData)
        }

        fetchUserData();
    }, []);

    return (
        <nav className="navbar">
            <div className="navbar-leftpart">
                <a className="navbar-logo" href="#">
                    <img src={Logo} width="auto" height="40" alt="logo" />
                </a>
                <button className="navbar-hotline btn-hotline">
                    <Phone size="20" />
                    <a href="tel:066268099862" className="hotline-tel"><span>  {t('header.hotline')} <span className="tel"> coming soon </span></span> </a>
                    <Info size="20" />
                </button>
            </div>
            <div className="navbar-buttons">
                <button type="button" className="btn btn-login">{t('header.greeting')} {userData} {t('header.login')}</button>
                <button type="button" className="btn btn-helper">{t('header.register')} </button>
            </div>
        </nav>
    );
}

export default Header;